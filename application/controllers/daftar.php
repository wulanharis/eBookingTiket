<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Web extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('m_data');
	}
 
	public function index(){		
		$data['judul'] = "Halaman depan";
		$data['pelanggan_baru'] = $this->m_data->tampil_data()->result();
		$this->load->view('v_index',$data);
	}
	
 
}


 
 
class Daftar extends CI_Controller{
 
	function __construct(){
		parent::__construct();		
		$this->load->model('m_data');
		$this->load->helper('url');
 
	}
 
	function index(){
		$data['daftar_pelanggan'] = $this->m_data->tampil_data()->result();
		$this->load->view('v_tampil',$data);
	}
 
	function tambah(){
		$this->load->view('v_input');
	}
	function tambah_aksi(){
		$id = $this->input->post('id');
		$username = $this->input->post('username');
		$pass = $this->input->post('pass');
		$nama_lengkap = $this->input->post('nama_lengkap');
		$alamat = $this->input->post('alamat');
		$kota = $this->input->post('kota');
		$telepon = $this->input->post('telepon');
		
		
		$data = array(
			'id' => $id,
			'username' => $username,
			'pass' => $pass,
			'nama_lengkap' => $nama_lengkap,
			'alamat' => $alamat,
			'kota' => $kota,
			'telepon' => $telepon
			
			);
		$this->m_data->input_data($data,'daftar_pelanggan');
		redirect('index.php/daftar/index');
	}
}


