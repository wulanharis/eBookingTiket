<!DOCTYPE html>
  <html>
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <title><?php echo $pageTitle; ?></title>
      <style>
        body {
          width: 100%;
          height: 100%;
          padding: 0px;
          margin: 0px;
        }
        
        header {
          width: 100%;
          background-color: red;
        }
        
        footer {
          width: 100%;
          background-color: orange;
        }
 
        #content {
          width: 69%;
          background-color: blue;
          display: inline-block;
        }
 
        #sidebar {
          width: 30%;
          background-color: green;
          display: inline-block;
        }
      </style>
    </head>
    <body>
      <header>
        <h1>Belajar Template Codeigniter</h1>
      </header>
      <div class="container">