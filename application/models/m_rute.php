<?php
 
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class M_rute extends CI_Model
{
    // get data dropdown
    function dd_rute()
    {
        // ambil data dari db
        $this->db->order_by('rute', 'asc');
        $result = $this->db->get('rute');
        
        // bikin array
        // please select berikut ini merupakan tambahan saja agar saat pertama
        // diload akan ditampilkan text please select.
        $dd[''] = 'Please Select';
        if ($result->num_rows() > 0) {
            foreach ($result->result() as $row) {
            // tentukan value (sebelah kiri) dan labelnya (sebelah kanan)
                $dd[$row->id_rute] = $row->rute;
            }
        }
        return $dd;
    }
}
 